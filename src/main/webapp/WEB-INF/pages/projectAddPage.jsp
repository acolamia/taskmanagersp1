<%--
  Created by IntelliJ IDEA.
  User: agris
  Date: 03.12.2019
  Time: 21:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>TASK MANAGER</title>
    <style>
        body {
            margin: 1%;
        }
    </style>
</head>
<body>
<form:form method="POST" action="/addProject" modelAttribute="project">
    <p>
    <h4 align="left">EDIT NAME</h4>
    </p>
    <p>
    <div align="left" class="col-3">
        <form:input cssClass="form-control" path="name"/>
    </div>
    </p>
    <p>
    <h4 align="left">EDIT DESCRIPTION</h4>
    </p>
    <div align="left" class="col-3">
        <form:input cssClass="form-control" path="description"/>
    </div>
    <p>
    <h4 align="left">EDIT START DATE</h4>
    </p>
    <p>
    <div align="left" class="col-3">
        <form:input type="date" cssClass="form-control" path="dateBegin"/>
    </div>
    </p>
    <p>
    <h4 align="left">EDIT FINISH DATE</h4>
    </p>
    <p>
    <div align="left" class="col-3">
        <form:input type="date" cssClass="form-control" path="dateEnd"/>
    </div>
    </p>
    <p>
    <h4 align="left">EDIT STATUS</h4>
    </p>
    <p>
    <div align="left" class="col-3">
        <select class="form-control" name="statusType" id="statusType">
            <c:forEach var="status" items="${statusList}">
                <c:if test="${task.statusType == status}">
                    <option selected value="${status}">${status.displayName}</option>
                </c:if>
                <c:if test="${task.statusType != status}">
                    <option value="${status}">${status.displayName}</option>
                </c:if>
            </c:forEach>
        </select>
    </div>
    </p>
    <p>
        <button type="submit" class="btn btn-success">ADD PROJECT</button>
    </p>
</form:form>
</body>
</html>