<%--
  Created by IntelliJ IDEA.
  User: agris
  Date: 03.12.2019
  Time: 16:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>TASK MANAGER</title>
    <style type="text/css">
        a{
            text-decoration: none;
        }
        a:hover {
            text-decoration: none;
        }
    </style>
</head>
<body>
<br>
<br>
<br>
<div class="container">
    <div class="row justify-content-md-center">
        <div class="alert alert-primary" role="alert">
            <p><strong>
                <a href="/projectListPage"><h2 align="center"><font color="#8b0000">TASK MANAGER</font> </h2></a>
            </strong></p>
        </div>
    </div>
</div>
</body>
</html>
