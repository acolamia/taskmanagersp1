package ru.iteco.vetoshnikov.taskmanager.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.iteco.vetoshnikov.taskmanager.api.IProjectService;
import ru.iteco.vetoshnikov.taskmanager.api.ITaskService;
import ru.iteco.vetoshnikov.taskmanager.constant.StatusType;
import ru.iteco.vetoshnikov.taskmanager.model.Project;
import ru.iteco.vetoshnikov.taskmanager.model.Task;

import java.util.List;

@Controller
public class TaskController {
    @Autowired
    @NotNull
    private IProjectService projectService;
    @Autowired
    @NotNull
    private ITaskService taskService;
    @Nullable
    private Project project;

    @GetMapping(value = "/taskList/{id}")
    public ModelAndView allTasksOfProject(@PathVariable("id") @NotNull final String id) {
        project = projectService.getById(id);
        @NotNull final List<Task> taskList = taskService.allTasks(project.getId());
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("taskListPage");
        modelAndView.addObject("taskList", taskList);
        modelAndView.addObject("project", project);
        return modelAndView;
    }

    @GetMapping(value = "/editTask/{id}")
    public ModelAndView editTaskPage(@PathVariable("id") @NotNull final String id) {
        @NotNull final Task task = taskService.getById(id);
        @NotNull final List<Project> projectList = projectService.allProjects();
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("taskEditPage");
        modelAndView.addObject("statusList", StatusType.values());
        modelAndView.addObject("task", task);
        modelAndView.addObject("projectList", projectList);
        return modelAndView;
    }

    @PostMapping(value = "/editTask")
    public ModelAndView editTask(@ModelAttribute("task") @NotNull final Task task) {
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/taskList/" + project.getId());
        taskService.edit(task);
        return modelAndView;
    }


    @GetMapping(value = "/addTask")
    public ModelAndView addTaskPage() {
        @NotNull final Task task = new Task();
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("taskAddPage");
        modelAndView.addObject("statusList", StatusType.values());
        modelAndView.addObject("task", task);
        return modelAndView;
    }

    @PostMapping(value = "/addTask")
    public ModelAndView addTask(@ModelAttribute("task") @NotNull final Task task) {
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/taskList/" + project.getId());
        task.setProjectId(project.getId());
        taskService.add(task);
        return modelAndView;
    }

    @GetMapping(value = "/deleteTask/{id}")
    public ModelAndView deleteTask(@PathVariable("id") @NotNull final String id) {
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/taskList/" + project.getId());
        @NotNull final Task task = taskService.getById(id);
        taskService.delete(task);
        return modelAndView;
    }
}
