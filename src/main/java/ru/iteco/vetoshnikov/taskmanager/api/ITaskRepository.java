package ru.iteco.vetoshnikov.taskmanager.api;

import ru.iteco.vetoshnikov.taskmanager.model.Task;

import java.util.List;

public interface ITaskRepository {
    List<Task> allTasks(String projectId);

    void add(Task task);

    void delete(Task task);

    void edit(Task task);

    Task getByProjectIdAndId(String projectId, String id);

    Task getById(String id);

    void deleteAllByProjectId(String projectId);
}
