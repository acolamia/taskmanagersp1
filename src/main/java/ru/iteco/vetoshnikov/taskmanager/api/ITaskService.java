package ru.iteco.vetoshnikov.taskmanager.api;

import ru.iteco.vetoshnikov.taskmanager.model.Task;

import java.util.List;

public interface ITaskService {
    List<Task> allTasks(String projectId);

    void add(Task task);

    void delete(Task task);

    void deleteAllByProjectId(String projectId);

    void edit(Task task);

    Task getByProjectIdAndId(String projectId, String id);

    Task getById(String id);
}
