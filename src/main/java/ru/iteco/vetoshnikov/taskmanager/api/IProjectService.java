package ru.iteco.vetoshnikov.taskmanager.api;

import ru.iteco.vetoshnikov.taskmanager.model.Project;

import java.util.List;

public interface IProjectService {
    List<Project> allProjects();

    void add(Project project);

    void delete(Project project);

    void edit(Project project);

    Project getById(String id);
}
