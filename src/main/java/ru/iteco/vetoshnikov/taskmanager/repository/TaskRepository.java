package ru.iteco.vetoshnikov.taskmanager.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.iteco.vetoshnikov.taskmanager.api.ITaskRepository;
import ru.iteco.vetoshnikov.taskmanager.model.Task;

import java.util.*;

@NoArgsConstructor
@Repository
public class TaskRepository implements ITaskRepository {
    @NotNull
    private final static Map<String, Task> taskMap = new HashMap<>();

    @Nullable
    @Override
    public List<Task> allTasks(@NotNull final String projectId) {
        @NotNull final List<Task> taskList = new ArrayList<>();
        for (@Nullable final Task task : taskMap.values()) {
            if (task.getProjectId() == null || task.getProjectId().isEmpty()) continue;
            if (projectId.equals(task.getProjectId())) {
                taskList.add(task);
            }
        }
        return taskList;
    }

    @Override
    public void add(@NotNull final Task task) {
        taskMap.put(task.getId(), task);
    }

    @Override
    public void delete(@NotNull final Task task) {
        taskMap.remove(task.getId());
    }

    @Override
    public void deleteAllByProjectId(@NotNull final String projectId) {
        Iterator<Map.Entry<String, Task>> entryIterator = taskMap.entrySet().iterator();
        while (entryIterator.hasNext()) {
            Map.Entry<String, Task> entry = entryIterator.next();
            if (projectId.equals(entry.getValue().getProjectId())) {
                entryIterator.remove();
            }
        }
    }

    @Override
    public void edit(@NotNull final Task task) {
        taskMap.put(task.getId(), task);
    }

    @Nullable
    @Override
    public Task getByProjectIdAndId(@NotNull final String projectId, @NotNull final String id) {
        for (@Nullable final Task task : taskMap.values()) {
            if ((task != null && task.getProjectId() == null) || (task != null && task.getProjectId().isEmpty()))
                continue;
            if (projectId.equals(task.getProjectId()) && id.equals(task.getId())) {
                return task;
            }
        }
        return null;
    }

    @Nullable
    @Override
    public Task getById(@NotNull final String id) {
        return taskMap.get(id);
    }
}
