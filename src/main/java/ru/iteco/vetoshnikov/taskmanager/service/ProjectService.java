package ru.iteco.vetoshnikov.taskmanager.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.iteco.vetoshnikov.taskmanager.api.IProjectService;
import ru.iteco.vetoshnikov.taskmanager.model.Project;
import ru.iteco.vetoshnikov.taskmanager.api.IProjectRepository;

import java.util.List;

@NoArgsConstructor
@Service
public class ProjectService implements IProjectService {
    @NotNull
    @Autowired
    private IProjectRepository projectRepository;

    @Nullable
    @Override
    public List<Project> allProjects() {
        return projectRepository.allProjects();
    }

    @Override
    public void add(@Nullable final Project project) {
        if (project==null)return;
        projectRepository.add(project);
    }

    @Override
    public void delete(@Nullable final Project project) {
        if (project==null)return;
        projectRepository.delete(project);
    }

    @Override
    public void edit(@Nullable final Project project) {
        if (project==null)return;
        projectRepository.edit(project);
    }

    @Nullable
    @Override
    public Project getById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.getById(id);
    }
}
