package ru.iteco.vetoshnikov.taskmanager.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.iteco.vetoshnikov.taskmanager.api.ITaskService;
import ru.iteco.vetoshnikov.taskmanager.model.Task;
import ru.iteco.vetoshnikov.taskmanager.api.ITaskRepository;

import java.util.List;

@NoArgsConstructor
@Service
public class TaskService implements ITaskService {
    @NotNull
    @Autowired
    private ITaskRepository taskRepository;

    @Nullable
    @Override
    public List<Task> allTasks(@Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        return taskRepository.allTasks(projectId);
    }

    @Override
    public void add(@Nullable final Task task) {
        if (task == null) return;
        taskRepository.add(task);
    }

    @Override
    public void delete(@Nullable final Task task) {
        if (task == null) return;
        taskRepository.delete(task);
    }

    @Override
    public void deleteAllByProjectId(@Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) return;
        taskRepository.deleteAllByProjectId(projectId);
    }

    @Override
    public void edit(@Nullable final Task task) {
        if (task == null) return;
        taskRepository.edit(task);
    }

    @Nullable
    @Override
    public Task getByProjectIdAndId(@Nullable final String projectId, String id) {
        if (projectId == null || projectId.isEmpty() || id == null || id.isEmpty()) return null;
        return taskRepository.getByProjectIdAndId(projectId, id);
    }

    @Nullable
    @Override
    public Task getById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.getById(id);
    }
}
